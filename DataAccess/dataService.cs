﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DataAccess
{
    public class DataService
    {
        public SqlConnectionStringBuilder ConnectStringBuilder(string login="", string password="")
        {
            SqlConnectionStringBuilder connectionBuilder = new SqlConnectionStringBuilder();
            connectionBuilder.DataSource = @"./";
            connectionBuilder.InitialCatalog = "MyDB";
            //connectionBuilder.IntegratedSecurity = true;
            connectionBuilder.UserID = login;
            connectionBuilder.Password = password;
            return connectionBuilder;
        }
        public void DbConnect(SqlConnectionStringBuilder connectionBuilder)
        {
            using (SqlConnection _connection = new SqlConnection())
            {
                _connection.ConnectionString = connectionBuilder.ConnectionString;
                try
                {
                    _connection.Open();
                    MessageBox.Show($"Connection: {_connection.State}");
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message);
                    _connection.Dispose();
                }
                Console.ReadLine();
            }
        }
    }
}
